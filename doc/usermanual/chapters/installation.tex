\chapter{Installation}
\label{ch:installation}

This section aims to provide details and instructions on how to build and install \corry.
An overview of possible build configurations is given.
After installing and loading the required dependencies, there are various options to customize the installation of \corry.
This chapter contains details on the standard installation process and information about custom build configurations.

\section{Supported Operating Systems}
\label{sec:os}
\corry is designed to run without issues on either a recent Linux distribution or Mac OS\,X.
Furthermore, the continuous integration of the project ensures correct building and functioning of the software framework on CentOS\,7 (with GCC and LLVM), SLC\,6 (with GCC and LLVM) and Mac OS Sierra (OS X 10.12, with AppleClang).

\section{CMVFS}
\label{sec:cvmfs}
The software is automatically deployed to CERN's VM file system (CVMFS)~\cite{cvmfs} for every new tag.
In addition, the \parameter{master} branch is built and deployed every night.
New versions are published to the folder \dir{/cvmfs/clicdp.cern.ch/software/corryvreckan/} where a new folder is created for every new tag, while updates via the \parameter{master} branch are always stored in the \dir{latest} folder.

The deployed version currently comprises all modules which are active by default and do not require additional dependencies.
A \file{setup.sh} is placed in the root folder of the respective release, which allows to set up all runtime dependencies necessary for executing this version.
Versions both for SLC\,6 and CentOS\,7 are provided.

\section{Docker}
\label{sec:docker}
Docker images are provided for the framework to allow anyone to run analyses without the need of installing \corry on their system.
The only required program is the Docker executable, all other dependencies are provided within the Docker images.
In order to exchange configuration files and output data between the host system and the Docker container, a folder from the host system should be mounted to the container's data path \dir{/data}, which also acts as the Docker \parameter{WORKDIR} location.

The following command creates a container from the latest Docker image in the project registry and start an interactive shell session with the \command{corry} executable already in the \texttt{\$PATH}.
Here, the current host system path is mounted to the \dir{/data} directory of the container.

\begin{verbatim}
$ docker run --interactive --tty                               \
             --volume "$(pwd)":/data                           \
             --name=corryvreckan                               \
             gitlab-registry.cern.ch/corryvreckan/corryvreckan \
             bash
\end{verbatim}

Alternatively it is also possible to directly start the simulation instead of an interactive shell, e.g. using the following command:
\begin{verbatim}
$ docker run --tty --rm                                        \
             --volume "$(pwd)":/data                           \
             --name=corryvreckan                               \
             gitlab-registry.cern.ch/corryvreckan/corryvreckan \
             "corry -c my_analysis.conf"
\end{verbatim}
where an analysis described in the configuration \file{my_analysis.conf} is directly executed and the container terminated and deleted after completing the data processing.
This closely resembles the behavior of running \corry natively on the host system.
Of course, any additional command line arguments known to the \command{corry} executable described in Section~\ref{sec:executable} can be appended.

For tagged versions, the tag name should be appended to the image name, e.g.\ \parameter{gitlab-registry.cern.ch/corryvreckan/corryvreckan:v1.0}, and a full list of available Docker containers is provided via the project's container registry~\cite{corry-container-registry}.

\section{Binaries}

Binary release tarballs are deployed to EOS to serve as downloads from the web to the directory \dir{/eos/project/c/corryvreckan/www/releases}.
New tarballs are produced for every tag as well as for nightly builds of the \parameter{master} branch, which are deployed with the name \file{allpix-squared-latest-<system-tag>-opt.tar.gz}.


\section{Compilation from Source}

The following paragraphs describe how to compile the \corry framework and its individual analysis and reconstruction modules from the source code.

\subsection{Prerequisites}
\label{sec:prerequisites}
The core framework is compiled separately from the individual modules and \corry has therefore only one required dependency: ROOT 6 (versions below 6 are not supported)~\cite{root}.
Please refer to~\cite{rootinstallation} for instructions on how to install ROOT.
ROOT has several components of which the GenVector package is required to run \corry, a package included in the default build.

\subsection{Downloading the source code}
The latest version of \corry can be downloaded from the CERN Gitlab repository~\cite{corry-repo}.
For production environments it is recommended to only download and use tagged software versions, as many of the available git branches are considered development versions and might exhibit unexpected behavior.

For developers, it is recommended to always use the latest available version from the git \texttt{master} branch.
The software repository can be cloned as follows:

\begin{verbatim}
$ git clone https://gitlab.cern.ch/corryvreckan/corryvreckan.git
$ cd corryvreckan
\end{verbatim}

\subsection{Configuration via CMake}
\label{sec:cmake_config}
\corry uses the CMake build system to configure, build and install the core framework as well as all modules.
An out-of-source build is recommended: this means CMake should not be directly executed in the source folder.
Instead, a \textit{build} folder should be created, from which CMake should be run.
For a standard build without any additional flags this implies executing:

\begin{verbatim}
$ mkdir build
$ cd build
$ cmake ..
\end{verbatim}

CMake can be run with several extra arguments to change the type of installation.
These options can be set with -D\textit{option}.
The following options are noteworthy:
\begin{itemize}
\item \parameter{CMAKE_INSTALL_PREFIX}: The directory to use as a prefix for installing the binaries, libraries and data.
Defaults to the source directory (where the folders \textit{bin/} and \textit{lib/} are added).
\item \parameter{CMAKE_BUILD_TYPE}: Type of build to install, defaults to \parameter{RelWithDebInfo} (compiles with optimizations and debug symbols).
Other possible options are \texttt{Debug} (for compiling with no optimizations, but with debug symbols and extended tracing using the Clang Address Sanitizer library) and \texttt{Release} (for compiling with full optimizations and no debug symbols).
\item \textbf{\texttt{BUILD\_\textit{ModuleName}}}: If the specific module \parameter{ModuleName} should be installed or not.
Defaults to \texttt{ON} for most modules, however some modules with additional dependencies such as EUDAQ~\cite{eudaq} are disabled by default.
This set of parameters allows to configure the build for minimal requirements as detailed in Section~\ref{sec:prerequisites}.
\item \parameter{BUILD_ALL_MODULES}: Build all included modules, defaulting to \parameter{OFF}.
This overwrites any selection using the parameters described above.
\end{itemize}

An example of a custom debug build, without the \parameter{EventLoaderCLICpix2} module and with installation to a custom directory is shown below:
\begin{verbatim}
$ mkdir build
$ cd build
$ cmake -DCMAKE_INSTALL_PREFIX=../install/ \
        -DCMAKE_BUILD_TYPE=DEBUG \
        -DBUILD_EventLoaderCLICpix2=OFF ..
\end{verbatim}

\subsection{Compilation and installation}
Compiling the framework is now a single command in the build folder created earlier (replacing \textit{\textless number\_of\_cores\textgreater} with the number of cores to use for compilation):
\begin{verbatim}
$ make -j<number_of_cores>
\end{verbatim}
The compiled (non-installed) version of the executable can be found at \textit{src/exec/corry} in the \dir{build} folder.
Running \corry directly without installing can be useful for developers.
It is not recommended for normal users, because the correct library and model paths are only fully configured during installation.

To install the library to the selected installation location (defaulting to the source directory of the repository) requires the following command:
\begin{verbatim}
$ make install
\end{verbatim}

The binary is now available as \textit{bin/corry} in the installation directory.
