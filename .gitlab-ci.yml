variables:
    EOS_PATH: "/eos/project/c/corryvreckan/www/"
    DOCKER_FILE: etc/docker/Dockerfile

stages:
    - compilation
    - testing
    - formatting
    - documentation
    - packaging
    - deployment

#######################
# Compilation targets #
#######################

cmp:slc6-gcc:
    stage: compilation
    tags:
        - docker
    image: clicdp/slc6-base
    script:
        - export COMPILER_TYPE="gcc"
        - source .gitlab-ci.d/init_x86_64.sh
        - source .gitlab-ci.d/load_deps.sh
        - mkdir build
        - cd build
        - cmake -GNinja -DCMAKE_BUILD_TYPE=RELEASE -DROOT_DIR=$ROOTSYS ..
        - ninja
        - ninja install
    artifacts:
        paths:
            - build
            - bin
            - lib
        expire_in: 3 hour

cmp:slc6-llvm:
    stage: compilation
    tags:
        - docker
    image: clicdp/slc6-base
    script:
        - export COMPILER_TYPE="llvm"
        - source .gitlab-ci.d/init_x86_64.sh
        - mkdir build
        - cd build
        - cmake -GNinja -DCMAKE_BUILD_TYPE=RELEASE -DROOT_DIR=$ROOTSYS ..
        - ninja
        - ninja install
    artifacts:
        paths:
            - build
            - bin
            - lib
        expire_in: 3 hour

cmp:cc7-gcc:
    stage: compilation
    tags:
        - docker
    image: clicdp/cc7-base
    script:
        - export COMPILER_TYPE="gcc"
        - source .gitlab-ci.d/init_x86_64.sh
        - source .gitlab-ci.d/load_deps.sh
        - mkdir build
        - cd build
        - cmake -GNinja -DCMAKE_BUILD_TYPE=RELEASE -DROOT_DIR=$ROOTSYS ..
        - ninja
        - ninja install
    artifacts:
        paths:
            - build
            - bin
            - lib
        expire_in: 3 hour

cmp:cc7-llvm:
    stage: compilation
    tags:
        - docker
    image: clicdp/cc7-base
    script:
        - export COMPILER_TYPE="llvm"
        - source .gitlab-ci.d/init_x86_64.sh
        - mkdir build
        - cd build
        - cmake -GNinja -DCMAKE_BUILD_TYPE=RELEASE -DROOT_DIR=$ROOTSYS ..
        - ninja
        - ninja install
    artifacts:
        paths:
            - build
            - bin
            - lib
        expire_in: 3 hour

cmp:mac1014-clang:
    stage: compilation
    tags:
        - mac
    script:
        - source .gitlab-ci.d/init_mac.sh
        - source .gitlab-ci.d/load_deps.sh
        - mkdir build
        - cd build
        - cmake -GNinja -DCMAKE_BUILD_TYPE=RELEASE -DROOT_DIR=$ROOTSYS -DCMAKE_USE_RELATIVE_PATHS=TRUE ..
        - ninja
        - ninja install
    artifacts:
        paths:
            - build
            - bin
            - lib
        expire_in: 3 hour

##############
# Unit tests #
##############

.test:
    stage: testing
    tags:
        - docker
    dependencies:
        - cmp:cc7-gcc
    image: clicdp/cc7-base
    before_script:
        - source .gitlab-ci.d/init_x86_64.sh

tst:telescope:
    extends: .test
    script:
        - cd build/
        - mkdir -p ../testing/data
        - ctest -R test_timepix3tel --output-on-failure -j4


############################
# Format and Lint Checking #
############################

# SLC 6

fmt:slc6-llvm-format:
    stage: formatting
    tags:
        - docker
    dependencies:
        - cmp:slc6-llvm
    image: clicdp/slc6-base
    script:
        - export COMPILER_TYPE="llvm"
        - source .gitlab-ci.d/init_x86_64.sh
        - cd build/
        - ninja check-format

fmt:slc6-llvm-lint:
    stage: formatting
    tags:
        - docker
    dependencies:
        - cmp:slc6-llvm
    image: clicdp/slc6-base
    script:
        - export COMPILER_TYPE="llvm"
        - source .gitlab-ci.d/init_x86_64.sh
        - cd build/
        - ninja check-lint

# CentOS 7

fmt:centos7-llvm-format:
    stage: formatting
    tags:
        - docker
    dependencies:
        - cmp:cc7-llvm
    image: clicdp/cc7-base
    script:
        - export COMPILER_TYPE="llvm"
        - source .gitlab-ci.d/init_x86_64.sh
        - cd build/
        - ninja check-format

fmt:cc7-llvm-lint:
    stage: formatting
    tags:
        - docker
    dependencies:
        - cmp:cc7-llvm
    image: clicdp/cc7-base
    script:
        - export COMPILER_TYPE="llvm"
        - source .gitlab-ci.d/init_x86_64.sh
        - cd build/
        - ninja check-lint


#############################
# Documentation Compilation #
#############################

# Compile Doxygen reference
cmp:doxygen:
    stage: documentation
    tags:
        - docker
    image: clicdp/cc7-base
    dependencies: []
    script:
        - source .gitlab-ci.d/init_x86_64.sh
        - source .gitlab-ci.d/load_deps.sh
        - mkdir -p public/usermanual
        - mkdir build
        - cd build
        - cmake -DBUILD_DOCS_ONLY=ON ..
        - make reference
        - mv reference/html ../public/reference
    artifacts:
        paths:
            - public
        expire_in: 24 hour

# Compile LaTeX user manual:
cmp:usermanual:
    stage: documentation
    tags:
      - docker
    image: gitlab-registry.cern.ch/clicdp/publications/templates/custom_ci_worker:fedora-latex-latest
    dependencies: []
    script:
        - mkdir -p public/usermanual
        - mkdir build
        - cd build
        - cmake -DBUILD_DOCS_ONLY=ON ..
        - make pdf
        - mv usermanual/corryvreckan-manual.pdf ../public/usermanual
    artifacts:
        paths:
          - public
        expire_in: 24 hour

################################
# Packaging of Binary Tarballs #
################################

pkg:slc6-gcc:
    stage: packaging
    tags:
        - docker
    image: clicdp/slc6-base
    only:
        - tags
        - schedules
    dependencies:
        - cmp:slc6-gcc
    script:
        - export COMPILER_TYPE="gcc"
        - source .gitlab-ci.d/init_x86_64.sh
        - source .gitlab-ci.d/load_deps.sh
        - mkdir -p public/releases
        - cd build
        - cmake -GNinja -DCMAKE_SKIP_RPATH=ON -DCMAKE_INSTALL_PREFIX=/tmp ..
        - ninja package
        - mv *.tar.gz ../public/releases
    artifacts:
        paths:
            - public
        expire_in: 24 hour

pkg:cc7-gcc:
    stage: packaging
    tags:
        - docker
    image: clicdp/cc7-base
    only:
        - tags
        - schedules
    dependencies:
        - cmp:cc7-gcc
    script:
        - export COMPILER_TYPE="gcc"
        - source .gitlab-ci.d/init_x86_64.sh
        - source .gitlab-ci.d/load_deps.sh
        - mkdir -p public/releases
        - cd build
        - cmake -GNinja -DCMAKE_SKIP_RPATH=ON -DCMAKE_INSTALL_PREFIX=/tmp ..
        - ninja package
        - mv *.tar.gz ../public/releases
    artifacts:
        paths:
            - public
        expire_in: 24 hour

########################
# Automatic Deployment #
########################

# Automatically deploy documentation to the website
# Deployment job only executed for new tag pushs, not for every commit.
deploy-documentation:
    stage: deployment
    tags:
      - docker
    variables:
        GIT_STRATEGY: none
    # Only run for new tags:
    only:
        - tags
    dependencies:
        - cmp:usermanual
        - cmp:doxygen
    # Docker image with tools to deploy to EOS
    image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest
    script:
        - deploy-eos
    # do not run any globally defined before_script or after_script for this step
    before_script: []
    after_script: []

deploy-eos:
    stage: deployment
    tags:
      - docker
    variables:
        GIT_STRATEGY: none
    # Only run for new tags:
    only:
        - tags
        - schedules # Only execute this on scheduled "nightly" pipelines
    dependencies:
        - pkg:cc7-gcc
        - pkg:slc6-gcc
    # Docker image with tools to deploy to EOS
    image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest
    script:
        - deploy-eos
    # do not run any globally defined before_script or after_script for this step
    before_script: []
    after_script: []

deploy-cvmfs:
    stage: deployment
    dependencies:
        - pkg:cc7-gcc
        - pkg:slc6-gcc
    tags:
        - cvmfs-deploy
    only:
        - tags
        - schedules # Only execute this on scheduled "nightly" pipelines
    script:
        - ./.gitlab-ci.d/download_artifacts.py $API_TOKEN $CI_PROJECT_ID $CI_PIPELINE_ID
        - export RUNNER_LOCATION=$(pwd)
        - if [ -z ${CI_COMMIT_TAG} ]; then export BUILD_PATH='latest'; else export BUILD_PATH=${CI_COMMIT_TAG}; fi
        - sudo -u cvclicdp -i $RUNNER_LOCATION/.gitlab-ci.d/gitlab_deploy.sh $RUNNER_LOCATION $BUILD_PATH
        - rm -f corryvreckan-*.tar.gz
    retry: 1

deploy-docker-latest:
    stage: deployment
    tags:
        - docker-image-build
    dependencies: []
    only:
        - schedules # Only execute this on scheduled "nightly" pipelines
    script:
        - "echo" # unused but this line is required by GitLab CI
    variables:
        TO: gitlab-registry.cern.ch/corryvreckan/corryvreckan

deploy-docker-tag:
    stage: deployment
    tags:
        - docker-image-build
    dependencies: []
    only:
        - tags
    script:
        - "echo" # unused but this line is required by GitLab CI
    variables:
        TO: gitlab-registry.cern.ch/corryvreckan/corryvreckan:${CI_COMMIT_TAG}
